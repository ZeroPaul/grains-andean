from django.contrib import admin
from material.admin.sites import site
from material.admin.decorators import register

from .models import Grain
from .models import TypeGrain
from .models import SampleGrain
from .models import AnalysisGrain

class AnalysisGrainInline(admin.TabularInline):
    model = AnalysisGrain
    extra = 0
class TypeGrainInline(admin.TabularInline):
    model = TypeGrain
    extra = 0

@register(SampleGrain)
class SampleGrainAdmin(admin.ModelAdmin):
    icon_name = 'track_changes'
    inlines = [AnalysisGrainInline, ]
    # list_display = ('field', )
    # fields = []                    
    # readonly_fields = ()                              
    # search_fields = [] 

@register(Grain)
class GrainAdmin(admin.ModelAdmin):
    icon_name='grain'
    inlines = [TypeGrainInline, ]


@register(TypeGrain)
class TypeGrainAdmin(admin.ModelAdmin):
    icon_name='blur_circular'


@register(AnalysisGrain)
class AnalysisGrainAdmin(admin.ModelAdmin):
    icon_name='search'

# site.register(Grain)
# site.register(TypeGrain)
# site.register(SampleGrain)
# site.register(AnalysisGrain)


