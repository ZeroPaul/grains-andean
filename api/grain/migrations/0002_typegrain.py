# Generated by Django 2.2.5 on 2019-10-01 16:50

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('grain', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TypeGrain',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False)),
                ('name_type', models.CharField(max_length=255)),
                ('status', models.BooleanField(default=True)),
                ('grain', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rgrain', to='grain.Grain')),
            ],
            options={
                'verbose_name_plural': 'TypeGrain',
                'ordering': ['name_type'],
            },
        ),
    ]
