import uuid as uuid_lib #pip install uuid
from django.utils.encoding import python_2_unicode_compatible
from django.db import models

@python_2_unicode_compatible # For Python 3.5+ and 2.7
class Grain(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid_lib.uuid4, editable=False)
    name = models.CharField(max_length=255)

    def __str__(self):
        return str(self.name)

    class Meta:
        #abstract = True
        ordering = ['name',]
        verbose_name_plural = 'Grains'

@python_2_unicode_compatible # For Python 3.5+ and 2.7
class TypeGrain(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid_lib.uuid4, editable=False)
    name_type = models.CharField(max_length=255)
    grain = models.ForeignKey(Grain, on_delete=models.CASCADE, related_name='grain_type')
    status = models.BooleanField(default=True)

    def __str__(self):
        return str(self.name_type)

    class Meta:
        #abstract = True
        ordering = ['name_type',]
        verbose_name_plural = 'Types Grains'

@python_2_unicode_compatible # For Python 3.5+ and 2.7
class SampleGrain(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid_lib.uuid4, editable=False)
    name_sample = models.CharField(max_length=255)
    image = models.ImageField(upload_to='samples/')
    grain = models.ForeignKey(Grain, on_delete=models.CASCADE, related_name='grain_sample')

    def __str__(self):
        return (str(self.grain.name) + ' | ' + str(self.name_sample))

    def save(self, *args, **kwargs):
        super(SampleGrain, self).save(*args, **kwargs)
        if self.image:
            pass
        
        types = TypeGrain.objects.filter(status=True, grain=self.grain.id)
        for type in range(len(types)):
            name_type_grain = str(types[type].name_type)
            name_type_grain = name_type_grain.replace(' ', '_').lower()

            id_type = types[type].id
            AnalysisGrain.objects.get_or_create(
                sample=self, type_grain=types[type], point_percent=1
            )
            print(id_type, name_type_grain)

            analysis = AnalysisGrain()
        print(self.grain.id)
        print(self.id)

    class Meta:
        #abstract = True
        ordering = ['name_sample',]
        verbose_name = 'Sample Grain'
        verbose_name_plural = 'Samples Grains'

@python_2_unicode_compatible # For Python 3.5+ and 2.7
class AnalysisGrain(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid_lib.uuid4, editable=False)
    sample = models.ForeignKey(SampleGrain, on_delete=models.CASCADE, \
                                  related_name='analysiss_sample')
    type_grain = models.ForeignKey(TypeGrain, on_delete=models.CASCADE, \
                                  related_name='analysis_type')
    point_percent = models.FloatField()
    image_result = models.ImageField(upload_to='sample_result/', blank=True)

    def __str__(self):
        return (str(self.sample.grain.name) + ' | ' + str(self.type_grain.name_type))

    # def save(self, *args, **kwargs):
    #     print("save")
    class Meta:
        #abstract = True
        ordering = ['sample__nsame_sample']
        verbose_name_plural = 'Analysis Grains'


